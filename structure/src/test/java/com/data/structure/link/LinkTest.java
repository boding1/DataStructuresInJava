/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.data.structure.link;

import junit.framework.TestCase;

/**
 * @author lengleng
 * @date 2018/10/9
 */
public class LinkTest extends TestCase {

    public void testAddNode() {
        Link link = new Link();
        link.addNode(1);
        link.addNode(2);
        link.addNode(3);
        link.addNode(4);
        link.addNode(5);

        System.out.println("link的长度 --> " + link.length());

        link.deleteNode(3);
        System.out.println("link的长度 --> " + link.length());
    }
}