/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.data.structure.link;

/**
 * @author lengleng
 * @date 2018/10/9
 */
public class Node {
    /**
     * 节点的引用，指向下一个节点
     */
    Node next = null;
    /**
     * 节点的对象，即内容
     */
    int data;

    public Node(int data) {
        this.data = data;
    }
}
