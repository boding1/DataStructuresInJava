package com.data.structure.day05;

import org.junit.Test;

public class EnumSingletonTest {
	@Test
	public void testSayHello() {
		System.out.println(EnumSingleton.INSTANCE == EnumSingleton.INSTANCE);
		EnumSingleton.INSTANCE.sayHello();
	}
}
