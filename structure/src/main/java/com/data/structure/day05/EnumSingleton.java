package com.data.structure.day05;

/**
 * 使用枚举实现单例
 * 
 * @author giegie
 * 
 */
public enum EnumSingleton {
	INSTANCE,User;
	public void sayHello() {
		System.out.println("hello world");
	}
}
