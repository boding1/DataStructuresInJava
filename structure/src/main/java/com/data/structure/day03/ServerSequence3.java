package com.data.structure.day03;

public class ServerSequence3 implements Sequence {
	private static ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>() {

		@Override
		protected Integer initialValue() {
			return 0;
		}

	};

	public int getNumber() {
		threadLocal.set(threadLocal.get() + 1);
		return threadLocal.get();
	}

	public static void main(String[] args) {
		Sequence sequence = new ServerSequence3();
		Thread thread1 = new Thread(new ClientThread3(sequence));
		Thread thread2 = new Thread(new ClientThread3(sequence));
		Thread thread3 = new Thread(new ClientThread3(sequence));
		thread1.start();
		thread2.start();
		thread3.start();
	}

}
