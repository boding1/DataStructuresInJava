package com.data.structure.day03;

public class ServerSequence implements Sequence {
	private static int num = 0;

	public int getNumber() {
		num = num + 1;
		return num;
	}

	public static void main(String[] args) {
		Sequence sequence = new ServerSequence();
		ClientThread thread1 = new ClientThread(sequence);
		ClientThread thread2 = new ClientThread(sequence);
		ClientThread thread3 = new ClientThread(sequence);
		thread1.start();
		thread2.start();
		thread3.start();
	}

}
