package com.data.structure.day03;

public class ClientThread3 implements Runnable {
	private Sequence sequence;

	public ClientThread3(Sequence sequence) {
		super();
		this.sequence = sequence;
	}

	public void run() {
		for (int i = 0; i < 3; i++) {
			System.out
					.println("[" + Thread.currentThread().getName() + "] ==>"+ sequence.getNumber());
		}
	}

}
