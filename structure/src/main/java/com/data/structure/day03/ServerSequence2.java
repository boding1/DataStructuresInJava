package com.data.structure.day03;

public class ServerSequence2 implements Sequence {
	private static ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>(){

		@Override
		protected Integer initialValue() {
			return 0;
		}
		
	};

	public int getNumber() {
		threadLocal.set(threadLocal.get() + 1);
		return threadLocal.get();
	}

	public static void main(String[] args) {
		Sequence sequence = new ServerSequence2();
		ClientThread thread1 = new ClientThread(sequence);
		ClientThread thread2 = new ClientThread(sequence);
		ClientThread thread3 = new ClientThread(sequence);
		thread1.start();
		thread2.start();
		thread3.start();
	}

}
