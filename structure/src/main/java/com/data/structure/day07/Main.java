package com.data.structure.day07;

public class Main {
	public static void main(String[] args) {
		CGlibProxy proxy = new CGlibProxy();
		Hello helloProxy = proxy.getProxy(HelloImpl.class);
		helloProxy.say();
	}
}
