package com.data.structure.day07;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author: wangjie19558
 * Date:2016/10/19
 * Description:
 */
public class TestDateFormate {
    @Test
    public void test1(){
        String dateString = "20161014082337";
        try
        {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
            Date date = sdf.parse(dateString);
            System.out.println(date);
        }
        catch (ParseException e)
        {
            System.out.println(e.getMessage());
        }

    }
}
