package com.data.structure.day02;

/**
 * abc 和 xyz比赛 a不能和x比 c不能和xyz比。。
 * 
 * @author giegie
 * @date 2016年5月22日
 * @description
 * 
 */
public class Test01 {
	public static void main(String[] args) {
		char s[] = { 'A', 'B', 'C' };
		char s1[] = { 'X', 'Y', 'Z' };

		for (int i = 0; i < s.length; i++) {
			for (int j = 0; j < s1.length; j++) {
				if (s[i] == 'A' && s1[j] == 'X')
					continue;
				if (s[i] == 'C' && (s1[j] == 'X' || s1[j] == 'Z'|| s1[j] == 'Y'))
					continue;
				System.out.println("甲队 " + s[i] + "------- 乙队"+ s1[j]);
			}
		}
	}
}
