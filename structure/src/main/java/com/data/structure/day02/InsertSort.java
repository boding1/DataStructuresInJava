package com.data.structure.day02;

/**
 * 插入排序
 * 
 * @author giegie
 * @date 2016年5月22日
 * @description
 * 
 */
public class InsertSort {
	public static void main(String[] args) {
		int a[] = { 2, 1, 5, 3, 9, 5, 4, 8 };
		// 插入排序
		for (int i = 0; i < a.length; i++) {
			int temp = a[i]; // 待插入数
			int j;

			// 将大于temp的往后移动一位
			for (j = i - 1; j >= 0; j--) {
				if (a[j] > temp) {
					a[j + 1] = a[j];
				} else {
					break;
				}
			}
			a[j + 1] = temp;
		}
		System.out.println("排序之后：");
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i] + " ");
		}
	}
}
