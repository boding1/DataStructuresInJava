package com.data.structure.day06;

/**
 * 枚举类应用学习
 * @author giegie
 * 
 */
public enum EnumLearn {
	USER1("test1", 123), USER2("test1", 123);
	private String name;
	private int age;

	EnumLearn(String name, int ordinal) {
		this.name = name;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public static EnumLearn getEnumByName(String name) {
		EnumLearn result = null;
		for (EnumLearn value : EnumLearn.values()) {
			result = value.getName().equals(name) ? value : null;
		}
		return result;
	}
}
